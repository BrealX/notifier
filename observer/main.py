"""
app:observer.main
"""

import asyncio
from collections import defaultdict
from typing import Any
from typing import Dict


from observer.signals import Notifier
from observer.signals import Subscriber


class RealNotifier(Notifier):
    """
    Real notifier who owns some important state and notifies his subscribers
    about its changes.
    """

    async def notify(self, **kwargs: Any) -> None:
        """
        Start updates in every subscriber.
        """
        print('Notifier: Notifying subscribers...')
        for subscriber in self._subscribers:
            await subscriber.update(self, **kwargs)

    async def some_business_logic(self, state: Dict[str, Any]) -> None:
        """
        Here we have custom business logic after (or before) which we need to
        notify subscribers about the notifier state.
        """

        print("Notifier: I'm doing something important.")
        self._state = defaultdict(bool, state)

        print(f'Notifier: My state has just changed to: {self._state}')
        await self.notify(any_parameters='foo')


class RealSubscriberA(Subscriber):
    """
    Imitation of the real subscriber.
    """
    async def update(self, _notifier: Notifier, **kwargs: Any) -> None:
        if _notifier.state['key_a']:
            print("RealSubscriberA: Reacted to the event")


class RealSubscriberB(Subscriber):
    """
    Imitation of the real subscriber.
    """
    async def update(self, _notifier: Notifier, **kwargs: Any) -> None:
        if _notifier.state['key_b']:
            print("RealSubscriberB: Reacted to the event")


class RealSubscriberC(Subscriber):
    """
    Imitation of the real subscriber.
    """
    async def update(self, _notifier: Notifier, **kwargs: Any) -> None:
        if _notifier.state['key_c']:
            print("RealSubscriberC: Reacted to the event")


if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    notifier = RealNotifier()

    subscriber_a = RealSubscriberA()
    loop.run_until_complete(notifier.attach(subscriber_a))

    subscriber_b = RealSubscriberB()
    loop.run_until_complete(notifier.attach(subscriber_b))

    loop.run_until_complete(
        notifier.some_business_logic(state={'key_a': True})
    )
    loop.run_until_complete(
        notifier.some_business_logic(state={'key_b': 1})
    )

    subscriber_c = RealSubscriberC()
    loop.run_until_complete(notifier.attach(subscriber_c))

    loop.run_until_complete(
        notifier.some_business_logic(state={'key_c': 1.0})
    )
