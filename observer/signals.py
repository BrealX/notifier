"""
app:observer.signals
"""

from __future__ import annotations

import logging
from abc import ABCMeta
from abc import abstractmethod
from collections import defaultdict
from typing import Any
from typing import Dict
from typing import List


class Notifier(metaclass=ABCMeta):
    """
    Interface of the Notifier updates the methods for subscribers management.
    """
    _state: Dict[str, bool]
    _subscribers = List[Any]

    def __init__(self):
        self._state = defaultdict(bool)
        self._subscribers = list()

    async def attach(self, subscriber: Subscriber) -> None:
        """
        Attach subscriber to the notifier.
        """
        logging.info(
            f'{self.__class__.__name__}: Subscriber '
            f'{subscriber.__class__.__name__} attached.'
        )
        self._subscribers.append(subscriber)

    async def detach(self, subscriber: Subscriber) -> None:
        """
        Detach subscriber from the notifier.
        """
        logging.info(
            f'{self.__class__.__name__}: Subscriber '
            f'{subscriber.__class__.__name__} detached.'
        )
        self._subscribers.remove(subscriber)

    @abstractmethod
    async def notify(self) -> None:
        """
        Notify all subscribers about some action.
        """
        raise NotImplementedError

    @property
    def state(self) -> Dict[str, bool]:
        """
        Return current notifier state.
        """
        return self._state


class Subscriber(metaclass=ABCMeta):
    """
    Interface of the Subscriber defines the method of notification
    which notifiers use for their subscribers notification.
    """

    @abstractmethod
    async def update(
        self, notifier: Notifier, **kwargs: Dict[Any, Any]
    ) -> None:
        """
        Get updates from notifier and do some business logic based on the
        state.
        """
        raise NotImplementedError
