"""
https://refactoring.guru/ru/design-patterns/iterator/python/example
"""

from __future__ import annotations
from collections.abc import Iterable
from collections.abc import Iterator
from typing import List


class EveryOddIterator(Iterator):
    """
    Custom iterator for numbers which return only odd values.
    """
    _position: int = None  # keeps the current iteration index
    _reverse: bool = False  # shows the direction of the iteration

    def __init__(
        self, collection: List[int], reverse: bool = False
    ) -> None:
        self._collection = collection
        self._reverse = reverse
        self._position = -1 if reverse else 0

    def __next__(self):
        """
        Returns the next element of the sequence. At the end of the sequence
        StopIteration exception must be raised.
        """
        try:
            value = self._collection[self._position]

            # custom restriction logic added here
            # return only multiples of 3
            while value % 3 != 0:
                self._position += -1 if self._reverse else 1
                value = self._collection[self._position]

            self._position += -1 if self._reverse else 1
        except IndexError:
            raise StopIteration()

        return value


class NumbersCollection(Iterable):
    _collection: List[int]
    _reverse: bool

    def __init__(self, collection: List[int], reverse: bool = False) -> None:
        self._collection = collection
        self._reverse = reverse

    def __iter__(self) -> EveryOddIterator:
        """
        Returns a custom iterator.
        """
        return EveryOddIterator(self._collection, self._reverse)


if __name__ == '__main__':
    coll = NumbersCollection(list(range(0, 101)))
    # coll = list(range(0, 101))

    for i in coll:
        print(i)
