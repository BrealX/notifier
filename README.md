This is a simple template on how to provide a notification system among
multiple application modules and packages.

It is based on the Observer behavioral design pattern and implements a simple
subscription system to notify object subscribes about any events that can
happen to the object they are subscribed to.
